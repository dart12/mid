import 'dart:io';
import 'dart:math';

import 'package:midterm/midterm.dart' as midterm;

void main() {
  List<String> list = [];
  String? ip = stdin.readLineSync();
  list = func1(ip);
  List<String> keep=postfix(list);
  print(evaluate_postfix(keep));
}
// Ex1
List<String> func1(var ip) {
  List<String> list = [];
  var sp = ip.split(" ");
  for (int i = 0; i < sp.length; i++) {
    if (sp[i] != "") {
      list.add(sp[i]);
    }
  }
  print(list);
  return list;
}
// Ex2
List<String> postfix(var ip){
  var value_frist=0,value_last=5;
  List<String> operator=[];
  List<String> pf=[];
  for (int i = 0; i < ip.length; i++) {
    if (ip[i] == "0" || ip[i] == "1" || ip[i] == "2" || ip[i] == "3" || ip[i] == "4" || ip[i] == "5" || ip[i] == "6" || ip[i] == "7" || ip[i] == "8" || ip[i] == "9") {
      pf.add(ip[i]);
    }
    if (ip[i] == "+" || ip[i] == "-" || ip[i] == "*" || ip[i] == "/" || ip[i] == "%" || ip[i] == "**") {
      value_frist=precedence(ip[i]);
      if (operator.isNotEmpty) {
        value_last=precedence(operator.last);
      }
      while (operator.isNotEmpty && operator.last != "(" && value_frist <= value_last) {
        pf.add(operator.removeLast());
      }
      operator.add(ip[i]);
    }
    if (ip[i] == "(") {
      operator.add(ip[i]);
    }
    if (ip[i] == ")") {
      while (operator.last != "(") {
        pf.add(operator.removeLast());
      }
      operator.remove("(");
    }
  }
  while (operator.isNotEmpty) {
    pf.add(operator.removeLast());
  }print(pf);
  return pf;
}

int precedence (var pre){
  var value=0;
  if ( pre == "(" || pre == ")") {
    value = 0;
  }if (pre == "+" || pre == "-") {
    value = 1;
  }if (pre == "*" || pre == "/") {
    value = 2;
  }if (pre == "%" || pre == "**"){
    value = 3;
  }
  return value;
}

double evaluate_postfix (List<String> postfix){
  List<double> values =[];
  double right,left;
  double sum=0;
  for(int i=0;i<postfix.length;i++){
    if(postfix[i]=="0"|| postfix[i]=="1"||postfix[i]=="2"||postfix[i]=="3"||postfix[i]=="4"||postfix[i]=="5"||postfix[i]=="6"||postfix[i]=="7"||postfix[i]=="8"||postfix[i]=="9"){
      if(postfix[i]=="0"){
        values.add(0);
      }if(postfix[i]=="1"){
        values.add(1);
      }if(postfix[i]=="2"){
        values.add(2);
      }if(postfix[i]=="3"){
        values.add(3);
      }if(postfix[i]=="4"){
        values.add(4);
      }if(postfix[i]=="5"){
        values.add(5);
      }if(postfix[i]=="6"){
        values.add(6);
      }if(postfix[i]=="7"){
        values.add(7);
      }if(postfix[i]=="8"){
        values.add(8);
      }if(postfix[i]=="9"){
        values.add(9);
      }
    }else{
      right=values.removeLast();
      left=values.removeLast();
      if(postfix[i]=="+"){
        sum = left+right;
      }if(postfix[i]=="-"){
        sum = left-right;
      }if(postfix[i]=="*"){
        sum = left*right;
      }if(postfix[i]=="/"){
        sum = left/right;
      }if(postfix[i]=="**"){
        sum = pow(left, right)+0.0;
      }if(postfix[i]=="%"){
        sum = left%right;
      }
      values.add(sum);
    }
  }
  return values[0];
}